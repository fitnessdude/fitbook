local composer = require( "composer" )
local widget = require("widget")
local scene = composer.newScene()

local com           = require "common"
local courseList    = require "courseList"
local taskList      = require "taskList"
-- local searchField   = require "searchField"
-- local example       = require "example"
-- -----------------------------------------------------------------------------
-- Code outside of the scene functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------
searching = false -- Not currently searching
--[[
imgDir contains the address of the image folder inside the Asset folder
--]]
local imgDir = "Assets/Images/"
local searchField = {}
searchField.searchTerm = "" -- Start with a blank searh criteria
local foundCountLabel
local wordCountlabel
local results = {}
local tableView

local function realTimeSearching()
  --[[
  real time searching
  --]]
  print("real time searching")
  foundGroup = display.newGroup()
  foundGroup.y = com.top + 60
  -- 2.
  lastTerm = "" -- No search term yet.
  curIndex = 1 -- On first word in word list.
  foundCount = 0 -- No words found yet.
  -- 3.
  searchTime = maxTime or (1000/display.fps/2)

  --Get the current search
  local searchTerm = getSearchTerm()
      local new_search = false
      if ( lastTerm ~= searchTerm ) then
          --result reset
          results = {}
          new_search = true
          print("different search term")
          lastTerm = searchTerm
          searching = ( string.len( searchTerm ) > 0 )
      end
      --Abort if not searching
      if ( not searching ) then
        return
      else print("searching")
      end

      --Search until we are out of time, or at the end of the word list
      local getTimer = system.getTimer -- localize for speedup
      local strLower = string.lower -- localize for speedup
      local startTime = getTimer()
      local elapsedTime = 0
      local results_counter = 0
      while ( elapsedTime < searchTime and curIndex <= #taskList ) do
          local curWord = taskList[curIndex]
          if ( string.match( strLower(curWord), strLower( searchTerm ) ) ~= nil ) then
              -- example.drawResult( curWord )
              results_counter = results_counter + 1
              print("Found Matching " .. curWord)
              table.insert(results, results_counter, curWord)
          end
          elapsedTime = getTimer() - startTime
          curIndex = curIndex + 1
      end
      --Update the search index
      -- searchField.setSearchIndex( curIndex )
      print("curIndex " .. curIndex)
      --Check to see if we reached the end of list, and quit if so
      searching = curIndex < #taskList
      if new_search then
        print("new results")
      else
        print("old results")
      end
      return results
end

local function startSearch(query)
  print("Start seraching")
  return realTimeSearching()
end

local function goHome()
        composer.gotoScene("Scenes.homePage", "slideRight", 200 )
    return true
end
local function goProfile()
        composer.gotoScene("Scenes.profilePage", "slideRight", 200 )
    return true
end

local function goCourse(courseTitle)
  local options = {
      effect = "slideLeft",
      time = 300,
      params = { courseTitle = courseTitle }
  }
  composer.gotoScene("Scenes.coursePage", options)
  return true
end
-- -----------------------------------------------------------------------------
-- Scene functions
-- -----------------------------------------------------------------------------

-- create()
function scene:create( event )

    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
            -- Code here runs when the scene is still off screen (but is about to come on screen)
        --[[
        Create background
        --]]
        local background = display.newImageRect(imgDir .. "background.jpg", com.right-com.left, com.bottom - com.top)
        background.x = (com.right-com.left)/2; background.y = (com.bottom-com.top)/2+com.top;
        sceneGroup:insert(background)
        background.alpha = 0.7

        ---------------------------------
       --[[
       insert bottom menu bar
       --]]
       local menuBar = display.newGroup()

       buttonHome = widget.newButton{
           defaultFile = imgDir .. "button/homeButtonPress.png",
           onRelease = goHome,
           width = display.contentWidth/4.0,
           height = display.contentWidth/4.0
       }
       buttonHome.y = com.bottom-buttonHome.height/2
       buttonHome.x = display.contentWidth/8
       menuBar:insert(buttonHome)

      --  buttonProfile = widget.newButton{
      --      defaultFile = imgDir .. "button/userButton.png",
      --      onRelease = goProfile,
      --      width = display.contentWidth/4.0,
      --      height = display.contentWidth/4.0,
      --  }
      --  buttonProfile.y = buttonHome.y
      --  buttonProfile.x = display.contentWidth/2
      --  -- buttonHome.x = buttonProfile.x + buttonProfile.width + buttonProfile.x
      --  menuBar:insert(buttonProfile)
       --
      --  buttonSearch = widget.newButton{
      --      defaultFile = imgDir .. "button/searchButton.png",
      --      onRelease = goSearch,
      --      width = display.contentWidth/4.0,
      --      height = display.contentWidth/4.0
      --  }
      --  buttonSearch.y = buttonProfile.y
      --  buttonSearch.x = buttonHome.x + buttonHome.width + buttonProfile.x
      --  menuBar:insert(buttonSearch)

       sceneGroup:insert(menuBar)

        -- --[[
        -- insert searchfield
        -- --]]
        local searchBar = display.newGroup()

        if( com.onWin ) then
          print("Windows Simulator")
          -- local fieldBack = display.newRect( com.right - 2, com.top + 2, com.fullw - 108, 25)
          local fieldBack = display.newRect( com.left + 2, com.top + 2, com.fullw - 108, 25)
          fieldBack.anchorX = 1
          fieldBack.anchorY = 0

          local currentTermLabel = display.newText( "<search term>",
                                                  fieldBack.x - fieldBack.contentWidth + 4,
                                                  fieldBack.y + fieldBack.contentHeight/2,
                                                  native.systemFont, 10 )
          currentTermLabel.anchorX = 0
          currentTermLabel:setFillColor(0,0,0)

          onKey = function( event )
            if( event.phase == "down" ) then
              if( event.descriptor == "deleteBack" ) then
                if( string.len(searchField.searchTerm) < 2 ) then
                  searchField.searchTerm = ""
                else
                  searchField.searchTerm =
                    string.sub( searchField.searchTerm, 1,
                      string.len(searchField.searchTerm) - 1)
                end
              else
                searchField.searchTerm = searchField.searchTerm .. event.descriptor
              end
                currentTermLabel.text = searchField.searchTerm
            end
          end
          Runtime:addEventListener( "key", onKey )

        else
          print("On Device or OS X Simulator")
          local width = com.fullw - 108
          -- local textField = native.newTextField( com.right - 2 - width/2, com.top + 2 + 25/2, com.fullw - 108, 25)
          local fieldBorder = display.newRect( com.fullw/2, com.top + 100, com.fullw - 51, 28)
          fieldBorder:setFillColor(0)
          local textField = native.newTextField( com.fullw/2, com.top + 100, com.fullw - 50, 25)
          textField.userInput = function(self, event)
            if ( event.phase == "began" ) then
            elseif ( event.phase == "ended" or event.phase == "submitted" ) then
            elseif ( event.phase == "editing" ) then
              searchField.searchTerm = event.text
              -- Start Search
              results = startSearch(textField.text)
              if results ~= nil then
                print("clear table")
                clear_table()

                print("populate table with search result")
                tableView = create_result_table()
                sceneGroup:insert(tableView)

                print("update fountcountlabel")
                foundCountLabel = update_found_count()
                searchBar:insert(foundCountLabel)

                for i = 1,#results do
                  print("results ".. i .. " " .. results[i])
                end
                  print("found " .. #results .. " results")
              end
            end
          end

          textField:addEventListener( "userInput" )
          --[[
          insert textfield into searchBar
          --]]
        searchBar:insert(fieldBorder)

        searchBar:insert(textField)
        print('insert textField')
        end
        -- Add counters
        --
        wordCountlabel = display.newText( "Total: " .. #courseList, com.left + 2 , com.top + 70, native.systemFont, 10 )
        wordCountlabel.anchorX = 0
        wordCountlabel:setFillColor( 1, 1, 1 )

        foundCountLabel = display.newText( "Found: ", com.right - 3, wordCountlabel.y, native.systemFont, 10)
        foundCountLabel.anchorX = 1
        foundCountLabel:setFillColor( 1, 1, 1 )


        --[[
        insert searchbar objectselements into display group searchBar
        --]]
        searchBar:insert(wordCountlabel)
        searchBar:insert(foundCountLabel)

        --[[
        insert serarch bar group into sceneGroup
        --]]
        sceneGroup:insert(searchBar)

        tableView = create_table()
        sceneGroup:insert(tableView)


end


-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase


    if ( phase == "will" ) then

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
    end
end

-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    if buttonHome then
        buttonHome:removeSelf()
        buttonHome = nil
    end
    -- Code here runs prior to the removal of scene's view
end

function getSearchTerm()
  print("SearchTerm = " .. searchField.searchTerm)
	return searchField.searchTerm
end

function getResults()
  return results
end

local function getRowTitle(row)
  if row == 1 then
    return "Category 1"
  elseif row == 21 then
    return "Category 2"
  elseif row < 21 then
    return taskList[row-1]
  elseif row > 21 then
    return taskList[row]
  end
end

local function onRowRender( event )
    -- Get reference to the row group
    local row = event.row

    -- Cache the row "contentWidth" and "contentHeight" because the row bounds can change as children objects are added
    local rowHeight = row.contentHeight
    local rowWidth = row.contentWidth

    local rowTitle = display.newText( row, getRowTitle(row.index), 0, 0, nil, 14 )

    rowTitle:setFillColor( 0 )
    -- Align the label left and vertically centered
    rowTitle.anchorX = 0
    rowTitle.x = 0
    rowTitle.y = rowHeight * 0.5
end

local function onRowRenderResult( event )
    -- Get reference to the row group
    local row = event.row

    -- Cache the row "contentWidth" and "contentHeight" because the row bounds can change as children objects are added
    local rowHeight = row.contentHeight
    local rowWidth = row.contentWidth

    local rowTitle = display.newText( row, getResults()[row.index], 0, 0, nil, 14 )

    rowTitle:setFillColor( 0 )
    -- Align the label left and vertically centered
    rowTitle.anchorX = 0
    rowTitle.x = 0
    rowTitle.y = rowHeight * 0.5
end

local function onRowTouch( event )
    local target = event.target
    local index = target.index
    goCourse(getRowTitle(index))
end

local function onRowTouchResult( event )
  local target = event.target
  local index = target.index
  goCourse(getResults()[index])
  print("get results index " .. index)
end
function create_table()
  --[[
  list view for search result
  --]]
  -- Create the table view widget
  local tableView = widget.newTableView(
      {
          left = 0,
          top = 100,
          height = 330,
          width = 320,
          onRowRender = onRowRender,
          onRowTouch = onRowTouch,
          listener = scrollListener
      }
  )
  -- Insert 40 rows
  for i = 1, #taskList do

      local isCategory = false
      local rowHeight = 36
      local rowColor = { default={ 1, 1, 1 }, over={ 1, 0.5, 0, 0.2 } }
      local lineColor = { 0.5, 0.5, 0.5 }

      -- Make some rows categories
      if ( i == 1 or i == 21 ) then
          isCategory = true
          rowHeight = 40
          rowColor = { default={ 0.8, 0.8, 0.8, 0.8 } }
          lineColor = { 1, 0, 0 }
      end

      -- Insert a row into the tableView
      tableView:insertRow(
          {
              isCategory = isCategory,
              rowHeight = rowHeight,
              rowColor = rowColor,
              lineColor = lineColor,
              params = {}  -- Include custom data in the row
          }
      )
  end
  return tableView
end

function create_result_table()
  --[[
  list view for search result
  --]]
  -- Create the table view widget
  local tableView = widget.newTableView(
      {
          left = 0,
          top = 100,
          height = 330,
          width = 320,
          onRowRender = onRowRenderResult,
          onRowTouch = onRowTouchResult,
          listener = scrollListener
      }
  )
  -- Insert
  for i = 1, #results do

      -- Insert a row into the tableView
      tableView:insertRow(
          {
              isCategory = isCategory,
              rowHeight = rowHeight,
              rowColor = rowColor,
              lineColor = lineColor,
              params = {}  -- Include custom data in the row
          }
      )
  end
  return tableView
end

function clear_table()
    tableView:deleteAllRows()
end

function update_found_count()
  foundCountLabel:removeSelf()
  local numberResult = #results
  local foundCountLabel = display.newText( "Found: " .. numberResult, com.right - 30, wordCountlabel.y, native.systemFont, 10)
  foundCountLabel:setFillColor(1,1,1 )
  return foundCountLabel
end
-- -----------------------------------------------------------------------------
-- Scene function listeners
-- -----------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------

return scene
