local composer = require( "composer" )
local widget = require("widget")
local scene = composer.newScene()
local com = require "common"
local progressRing = require("progressRing")

local progress
local process_circle
local percentage
local percentage_text
local imgDir = "Assets/Images/"

-- -----------------------------------------------------------------------------
-- Code outside of the scene functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------

--[[
assigns the return value to the field onRelease.
--]]
local function newclosure(func, ...)
    local args = {...}
    return function()
        return func(unpack(args));
    end
end

local function goSearch()
        composer.gotoScene("Scenes.searchPage", "slideLeft", 1 )
    return true
end

local function goProfile()
        composer.gotoScene("Scenes.profilePage", "slideRight", 1 )
    return true
end

local function getDailyTasks(dayIndex)
    local days = {"Sunday", "Monday", "Tuesday",
     "Wednesday", "Thursday", "Friday", "Saturday"}
    print("load daily tasks of " .. days[dayIndex])
    local selectedDay = days[dayIndex]
    local options = {
      effect = "fade",
      time = 300,
      params = { day = selectedDay }
    }
    composer.gotoScene("Scenes.dailyTasksPage", options)
  return true
end
-- -----------------------------------------------------------------------------
-- Scene functions
-- -----------------------------------------------------------------------------

-- create()
function scene:create( event )
    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
        local  gap = 45
        local background = display.newImageRect("Assets/Images/background.jpg", com.right-com.left, com.bottom - com.top)
        background.x = (com.right-com.left)/2; background.y = (com.bottom-com.top)/2+com.top;

        sceneGroup:insert(background)

        background.alpha = 1

        -- Calendar
        --[[
        load images of the logo for days in a week to be used as a button to bring up daily tasks
        --]]
        local calendar = display.newGroup()

        local y = 50

        sun = widget.newButton{
            defaultFile = imgDir .. "home_image/s.png",
            onRelease = newclosure(getDailyTasks, 1),
            width = 60,
            height = 60
        }
        sun.x = 22;
        sun.y = y

        mon = widget.newButton{
            defaultFile = imgDir .. "home_image/m.png",
            onRelease = newclosure(getDailyTasks, 2),
            width = 60,
            height = 60
        }
        mon.x = sun.x + gap;
        mon.y = y

        tues = widget.newButton{
            defaultFile = imgDir .. "home_image/t.png",
            onRelease = newclosure(getDailyTasks, 3),
            width = 60,
            height = 60
        }
        tues.x = mon.x + gap;
        tues.y = y

        wed = widget.newButton{
            defaultFile = imgDir .. "home_image/w.png",
            onRelease = newclosure(getDailyTasks, 4),
            width = 60,
            height = 60
        }
        wed.x = display.contentWidth/2;
        wed.y = y

        thurs = widget.newButton{
            defaultFile = imgDir .. "home_image/t.png",
            onRelease = newclosure(getDailyTasks, 5),
            width = 60,
            height = 60
        }
        thurs.x = wed.x + gap;
        thurs.y = y

        fri = widget.newButton{
            defaultFile = imgDir .. "home_image/f.png",
            onRelease = newclosure(getDailyTasks, 6),
            width = 60,
            height = 60
        }
        fri.x = thurs.x + gap;
        fri.y = y

        sat = widget.newButton{
            defaultFile = imgDir .. "home_image/s.png",
            onRelease = newclosure(getDailyTasks, 7),
            width = 60,
            height = 60
        }
        sat.x = fri.x + gap;
        sat.y = y

        --[[
        insert days buttons into calendar
        --]]
        calendar:insert(sun)
        calendar:insert(mon)
        calendar:insert(tues)
        calendar:insert(wed)
        calendar:insert(thurs)
        calendar:insert(fri)
        calendar:insert(sat)

        --[[
        insert the calendar group into the scene
        --]]
        sceneGroup:insert(calendar)

        -- Process Circle
        --[[
        Create circle to be used for displaying the current state of the process
        --]]
        ---------------------------------
        -- Create bottom menu bar
        local menuBar = display.newGroup()

        -- buttonHome = widget.newButton{
        --     defaultFile = imgDir .. "button/homeButtonPress.png",
        --     width = display.contentWidth/4.0,
        --     height = display.contentWidth/4.0
        -- }
        -- buttonHome.y = com.bottom-buttonHome.height/2
        -- buttonHome.x = display.contentWidth/8
        -- menuBar:insert(buttonHome)

        buttonProfile = widget.newButton{
            defaultFile = imgDir .. "button/userButton.png",
            onRelease = goProfile,
            width = display.contentWidth/4.0,
            height = display.contentWidth/4.0,
        }
        buttonProfile.y = com.bottom-buttonProfile.height/2
        buttonProfile.x = display.contentWidth/8
        -- buttonHome.x = buttonProfile.x + buttonProfile.width + buttonProfile.x
        menuBar:insert(buttonProfile)

        buttonSearch = widget.newButton{
            defaultFile = imgDir .. "button/searchButton.png",
            onRelease = goSearch,
            width = display.contentWidth/4.0,
            height = display.contentWidth/4.0
        }
        buttonSearch.y = buttonProfile.y
        buttonSearch.x = display.contentWidth - 40
        menuBar:insert(buttonSearch)

        sceneGroup:insert(menuBar)
end


-- show()
function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase
    --[[
    imgDir contains the address of the image folder inside the Asset folder
    --]]

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
        -- display.contentWidth
            -- Process Circle
        --[[
        Create circle to be used for displaying the current state of the process
        --]]
        process_circle = display.newImageRect(imgDir .. "home_image/gauge.png", 300, 300)
        process_circle.x = display.contentWidth/2
        process_circle.y = display.contentHeight/2 + display.contentHeight/24

        local emb_color =
        {
            highlight = { r=1, g=1, b=1 },
            shadow = { r=1, g=1, b=1 }
        }

        sceneGroup:insert(process_circle)

        progress = progressRing.new({
            radius = 113.8,
            bgColor = {0.44, 0.54, 0.6, 0.2},
            ringColor = {0.45, 0.7, 0.8, 1},
            ringDepth = 0.125,
            position = 0
            })

        -- local max_gauge = 0.93

        progress.x = display.contentWidth/2
        progress.y = display.contentHeight/2 + display.contentHeight/24
        progress.rotation = -180

        -- Change this to update percentage shown in homepage
        percentage_text = 28

        progress:goTo(percentage_text/100., 2000)

        sceneGroup:insert(progress)

        -- Animation for increasing progress

        function progress_increasing()
            transition.to(progress, {time = 2000, strokeWidth = 12})
        end

        -- Number percentage

        percentage = display.newText(percentage_text, display.contentWidth/2, display.contentHeight/2 + display.contentHeight/24, "Lato-Thin.ttf", 70)

        sceneGroup:insert(percentage)


    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
        progress:removeSelf()
        process_circle:removeSelf()
        percentage:removeSelf()

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    if buttonSearch then
        buttonSearch:removeSelf()
        buttonSearch = nil
    end
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------
-- Scene function listeners
-- -----------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------

return scene
