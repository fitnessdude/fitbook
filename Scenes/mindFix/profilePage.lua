local composer = require( "composer" )
local widget = require("widget")
local scene = composer.newScene()
local com = require "common"


-- -----------------------------------------------------------------------------
-- Code outside of the scene functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------

--[[
assigns the return value to the field onRelease.
--]]
local weightSet
local weight_txtfld
local set_weight_label
local submit_weight
local weight_stat
local imgDir = "Assets/Images/"

local function newclosure(func, ...)
    local args = {...}
    return function()
        return func(unpack(args));
    end
end

local function goSearch()
        composer.gotoScene("Scenes.searchPage", "slideLeft", 200 )
    return true
end

local function goHome()
        composer.gotoScene("Scenes.homePage", "slideLeft", 200 )
    return true
end

local function submitWeight()
    if(not setWeight) then
        set_weight_label.text = weight_txtfld.text .." kg"
        weight_txtfld:removeSelf()
        set_weight_label:toFront()
        submit_weight:removeSelf()
        setWeight = true
        submit_weight.x = com.right - submit_weight.width/2.0 - 10
        submit_weight.y = weight_label.y
    end
end

local function goTrophy()
        composer.gotoScene("Scenes.trophyPage", "crossFade", 300 )
    return true
end


-- -----------------------------------------------------------------------------
-- Scene functions
-- -----------------------------------------------------------------------------

-- create()
function scene:create( event )
    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
    -------------------------------

        local  gap = 45
        -- Code here runs when the scene is still off screen (but is about to come on screen)
        -- display.contentWidth
        local background = display.newImageRect("Assets/Images/background.jpg", com.right-com.left, com.bottom - com.top)
        background.x = (com.right-com.left)/2; background.y = (com.bottom-com.top)/2+com.top;

        sceneGroup:insert(background)

        background.alpha = 0.7

        ---------------------------------
        -- Create bottom menu bar
       local menuBar = display.newGroup()

        -- buttonProfile = widget.newButton{
        --     defaultFile = imgDir .. "button/userButton.png",
        --     width = display.contentWidth/4.0,
        --     height = display.contentWidth/4.0,
        -- }
        -- buttonProfile.y = com.bottom-buttonProfile.height/2
        -- menuBar:insert(buttonProfile)

        buttonHome = widget.newButton{
            defaultFile = imgDir .. "button/homeButtonPress.png",
            onRelease = goHome,
            width = display.contentWidth/4.0,
            height = display.contentWidth/4.0
        }
        buttonHome.y = com.bottom-buttonHome.height/2
        buttonHome.x = display.contentWidth - 40
        menuBar:insert(buttonHome)

        -- buttonSearch = widget.newButton{
        --     defaultFile = imgDir .. "button/searchButton.png",
        --     onRelease = goSearch,
        --     width = display.contentWidth/4.0,
        --     height = display.contentWidth/4.0
        -- }
        -- buttonSearch.y = buttonProfile.y
        -- buttonSearch.x = buttonHome.x + buttonHome.width + buttonProfile.x
        -- menuBar:insert(buttonSearch)

        sceneGroup:insert(menuBar)

        ---------------------------------
        -- local soon = display.newImageRect(imgDir .. "coming-soon.png",background.width,background.height/4)
        -- soon.x = (com.right-com.left)/2; soon.y = com.bottom - soon.height/2 - buttonHome.height - 30
        -- sceneGroup:insert(soon)

        ---------------------------------

 -- Set Profile Pic
        local profilePic = display.newImageRect(imgDir .. "profile/profilePic.jpg", 120 , 120);
        profilePic.x = profilePic.width/2 + 20
        profilePic.y = profilePic.height/2 + 20 + com.top
----------

        sceneGroup:insert(profilePic)

        weight_stat = display.newGroup()
        weight_label = display.newText( "Orginal Weight:", com.left + 60, profilePic.y+profilePic.height, native.systemFont, 15)


        submit_weight = widget.newButton{
          -- defaultFile = imgDir .."button/checkReleaseButton.png",
          -- overFile = imgDir.."button/checkPressButton.png",
          defaultFile = imgDir .. "button/checkReleaseButton.png",
          overFile = imgDir .. "button/checkPressButton.png",
          onRelease = submitWeight,
          width = 25,
          height = 25
        }

        submit_weight.x = com.right - submit_weight.width/2.0 - 10
        submit_weight.y = weight_label.y

        if(not setWeight)then
            weight_txtfld = native.newTextField(((submit_weight.x - submit_weight.width/2.0) + (weight_label.x+weight_label.width/2.0))/2.0, submit_weight.y, (submit_weight.x - submit_weight.width/2.0) - (weight_label.x+weight_label.width/2.0) - 20, submit_weight.width)
            set_weight_label = display.newText("",weight_txtfld.x,weight_txtfld.y,native.systemFont, 15)
            weight_stat:insert(submit_weight)
            weight_stat:insert(weight_txtfld)
            print('inserted')
        end

        weight_stat:insert(weight_label)
        weight_stat:insert(set_weight_label)

        sceneGroup:insert(weight_stat)
        print('insert stat')
        -------------------------------------------------------------

        trophyButton = widget.newButton{
        defaultFile = imgDir .. "button/trophyReleaseButton.png",
        overFile = imgDir .. "button/trophyPressButton.png",
        onRelease = goTrophy,
        width = profilePic.width/2.0,
        height = profilePic.height/2.0
    }
        trophyButton.x = (com.right + (profilePic.x + profilePic.width/2.0))/2.0
        trophyButton.y = profilePic.y
        sceneGroup:insert(trophyButton)
end


-- show()
function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase
    --[[
    imgDir contains the address of the image folder inside the Asset folder
    --]]

    if ( phase == "will" ) then

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
        print('hide')
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    if buttonSearch then
        buttonSearch:removeSelf()
        buttonSearch = nil
    end
    -- Code here runs prior to the removal of scene's view
end


-- -----------------------------------------------------------------------------
-- Scene function listeners
-- -----------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------

return scene
