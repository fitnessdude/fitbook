local composer = require( "composer" )
local widget = require("widget")
local scene = composer.newScene()
local com = require "common"

local sun = require("sunday")
local mon = require("monday")
local tue = require("tuesday")
local thu = require("wednesday")
local wed = require("thursday")
local fri = require("friday")
local sat = require("saturday")


local imgDir = "Assets/Images/"
local selected
local title_name
local headline

local sunday
local monday
local tuesday
local wednesday
local thursday
local friday
local saturday

local submit

-- if sun.list[1]

print("Status:  This is sun.list = "..tostring(sun.list[i]))
print("Status:  This is mon.list = "..tostring(mon.list[i]))
print("Status:  This is tue.list = "..tostring(tue.list[i]))
print("Status:  This is wed.list = "..tostring(wed.list[i]))
print("Status:  This is thu.list = "..tostring(thu.list[i]))
print("Status:  This is fri.list = "..tostring(fri.list[i]))
print("Status:  This is sat.list = "..tostring(sat.list[i]))


local function goSearch()
    composer.gotoScene("Scenes.searchPage", "slideRight", 300 )
  return true
end

local function goHome()
    composer.gotoScene("Scenes.homePage", "slideRight", 300 )
  return true
end


local function newclosure(func, ...)
    local args = {...}
    return function()
        return func(unpack(args));
    end
end

local function create_backButton()
    local backButton = widget.newButton{
        defaultFile = imgDir .. "material-design/png/go-back-left-arrow.png",
        onRelease = newclosure(goSearch),
        width = 35,
        height = 35
    }
    backButton.x = 25;
    backButton.y = 50;
    return backButton
end
local function update_headline()
    if headline ~= nil then headline:removeSelf() print("reload headline") end
    courseName = "CourseName"
    local headlineOptions = {
      text = params.courseTitle,
      x = display.contentWidth/2,
      y = 50,
      width = 200,
      align = "left",
      font = native.systemFont,
      fontSize = 20
    }
    headline = display.newText(headlineOptions)
    headline:setFillColor( 0, 0, 0 )
    return headline
end

-- -----------------------------------------------------------------------------
-- Code outside of the scene functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------

--[[
assigns the return value to the field onRelease.
--]]

-- -----------------------------------------------------------------------------
-- Scene functions
-- -----------------------------------------------------------------------------

-- create()
function scene:create( event )

    local sceneGroup = self.view

    -- Code here runs when the scene is first created but has not yet appeared on screen
        local  gap = 45
        local background = display.newImageRect(imgDir .. "background-white.jpg", com.right-com.left, com.bottom - com.top)
        background.x = (com.right-com.left)/2; background.y = (com.bottom-com.top)/2+com.top;

        sceneGroup:insert(background)

        background.alpha = 1
        selected = {nil,nil,nil,nil,nil,nil,nil}
        backButton = create_backButton()
        sceneGroup:insert(backButton)


        local function selectSunday()
            selected[1] = 1
            print(selected[1],selected[2],selected[3],selected[4],selected[5],selected[6],selected[7])
        end
        local function selectMonday()
            selected[2] = 1
            print(selected[1],selected[2],selected[3],selected[4],selected[5],selected[6],selected[7])
        end
        local function selectTuesday()
            selected[3] = 1
            print(selected[1],selected[2],selected[3],selected[4],selected[5],selected[6],selected[7])
        end
        local function selectWednesday()
            selected[4] = 1
            print(selected[1],selected[2],selected[3],selected[4],selected[5],selected[6],selected[7])
        end
        local function selectThursday()
            selected[5] = 1
            print(selected[1],selected[2],selected[3],selected[4],selected[5],selected[6],selected[7])
        end
        local function selectFriday()
            selected[6] = 1
            print(selected[1],selected[2],selected[3],selected[4],selected[5],selected[6],selected[7])
        end
        local function selectSaturday()
            selected[7] = 1
            print(selected[1],selected[2],selected[3],selected[4],selected[5],selected[6],selected[7])
        end


        local function submitTask()

            print("Status:  This is sun.list = "..tostring(sun.list[1]))
            -- print("Status:  This is mon.list = "..tostring(mon.list[2]))

            print("Status:  Title_name is ".. title_name)
            table.insert(sun.list,title_name)
            local new_contents
            local old_task
            local file
            local errorString

----------------------writing------------------------

--------------------------Sunday----------------------------
            if(selected[1])then
                print('sunday writing')
                path = system.pathForFile("sunday.lua", system.baseDirectory)
                file, errorString = io.open( path, "r" )

                ----------------- read file to gain original data ---------------------
                if not file then
                    -- Error occurred; output the cause
                    print( "File error: " .. errorString )
                    old_task = ""

                else
                    -- Read data from file
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. "\n" .. old_task )
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. " 2\n" .. old_task )
                    -- Close the file handle
                    io.close( file )
                end

                file = nil
                old_task = string.sub(old_task,14,-2)
                if(old_task ~= "") then
                    print('true')
                    old_task = ","..old_task
                end
                -- print(old_task)
                new_contents = "local task = {}\ntask.list = {'"..title_name.."'"..old_task.."}\nreturn task"
                print(new_contents..": new")
                file = nil
                errorString = nil
                old_task = nil


                --------------- Write back to file: done ---------------------

                file,errorString = io.open(path,"w")

                if file then
                    print('writing')
                    file:write(new_contents)
                    print('done')
                    io.close( file )
                else
                    print('false')
                    print(file)
                    print( "File error: " .. errorString )
                end
                file = nil
                errorString = nil
                new_contents = nil

            end
            print("finish check sun")


--------------------------Monday----------------------------
            if(selected[2])then
                print('monday writing')
                path = system.pathForFile("monday.lua", system.baseDirectory)
                file, errorString = io.open( path, "r" )

                ----------------- read file to gain original data ---------------------
                if not file then
                    -- Error occurred; output the cause
                    print( "File error: " .. errorString )
                    old_task = ""
                else
                    -- Read data from file
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. "\n" .. old_task )
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. " 2\n" .. old_task )
                    -- Close the file handle
                    io.close( file )
                end

                file = nil
                old_task = string.sub(old_task,14,-2)
                if(old_task ~= "") then
                    print('true')
                    old_task = ","..old_task
                end
                -- print(old_task)
                new_contents = "local task = {}\ntask.list = {'"..title_name.."'"..old_task.."}\nreturn task"
                print(new_contents..": new")

                file = nil
                errorString = nil
                old_task = nil


                --------------- Write back to file: done ---------------------

                file,errorString = io.open(path,"w")

                if file then
                    print('writing')
                    file:write(new_contents)
                    print('done')
                    io.close( file )
                else
                    print('false')
                    print(file)
                    print( "File error: " .. errorString )
                end
                file = nil
                errorString = nil
                new_contents = nil
            end

            print("finish check monday")
--------------------------Tuesday----------------------------
            if(selected[3])then
                print('Tuesday writing')
                path = system.pathForFile("tuesday.lua", system.baseDirectory)
                file, errorString = io.open( path, "r" )

                ----------------- read file to gain original data ---------------------
                if not file then
                    -- Error occurred; output the cause
                    print( "File error: " .. errorString )
                    old_task = ""
                else
                    -- Read data from file
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. "\n" .. old_task )
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. " 2\n" .. old_task )
                    -- Close the file handle
                    io.close( file )
                end

                file = nil
                old_task = string.sub(old_task,14,-2)
                if(old_task ~= "") then
                    print('true')
                    old_task = ","..old_task
                end
                -- print(old_task)
                new_contents = "local task = {}\ntask.list = {'"..title_name.."'"..old_task.."}\nreturn task"
                print(new_contents..": new")
                file = nil
                errorString = nil
                old_task = nil


                --------------- Write back to file: done ---------------------

                file,errorString = io.open(path,"w")

                if file then
                    print('writing')
                    file:write(new_contents)
                    print('done')
                    io.close( file )
                else
                    print('false')
                    print(file)
                    print( "File error: " .. errorString )
                end
                file = nil
                errorString = nil
                new_contents = nil
            end
            print("finish tuesday")
--------------------------Wednesday----------------------------
            if(selected[4])then
                print('wednesday writing')
                path = system.pathForFile("wednesday.lua", system.baseDirectory)
                file, errorString = io.open( path, "r" )

                ----------------- read file to gain original data ---------------------
                if not file then
                    -- Error occurred; output the cause
                    print( "File error: " .. errorString )
                    old_task = ""
                else
                    -- Read data from file
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. "\n" .. old_task )
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. " 2\n" .. old_task )
                    -- Close the file handle
                    io.close( file )
                end

                file = nil
                old_task = string.sub(old_task,14,-2)
                if(old_task ~= "") then
                    print('true')
                    old_task = ","..old_task
                end
                -- print(old_task)
                new_contents = "local task = {}\ntask.list = {'"..title_name.."'"..old_task.."}\nreturn task"
                file = nil
                errorString = nil
                old_task = nil


                --------------- Write back to file: done ---------------------

                file,errorString = io.open(path,"w")

                if file then
                    print('writing')
                    file:write(new_contents)
                    print('done')
                    io.close( file )
                else
                    print('false')
                    print(file)
                    print( "File error: " .. errorString )
                end
                file = nil
                errorString = nil
                new_contents = nil
            end
            print("finish wednesday")
--------------------------Thursday----------------------------
            if(selected[5])then
                print('thursday writing')
                path = system.pathForFile("thursday.lua", system.baseDirectory)
                file, errorString = io.open( path, "r" )

                ----------------- read file to gain original data ---------------------
                if not file then
                    -- Error occurred; output the cause
                    print( "File error: " .. errorString )
                    old_task = ""
                else
                    -- Read data from file
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. "\n" .. old_task )
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. " 2\n" .. old_task )
                    -- Close the file handle
                    io.close( file )
                end

                file = nil
                old_task = string.sub(old_task,14,-2)
                if(old_task ~= "") then
                    print('true')
                    old_task = ","..old_task
                end
                -- print(old_task)
                new_contents = "local task = {}\ntask.list = {'"..title_name.."'"..old_task.."}\nreturn task"
                file = nil
                errorString = nil
                old_task = nil


                --------------- Write back to file: done ---------------------

                file,errorString = io.open(path,"w")

                if file then
                    print('writing')
                    file:write(new_contents)
                    print('done')
                    io.close( file )
                else
                    print('false')
                    print(file)
                    print( "File error: " .. errorString )
                end
                file = nil
                errorString = nil
                new_contents = nil
            end
            print("finish thursday")
--------------------------Friday----------------------------
            if(selected[6])then
                print('friday writing')
                path = system.pathForFile("friday.lua", system.baseDirectory)
                file, errorString = io.open( path, "r" )

                ----------------- read file to gain original data ---------------------
                if not file then
                    -- Error occurred; output the cause
                    print( "File error: " .. errorString )
                    old_task = ""
                else
                    -- Read data from file
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. "\n" .. old_task )
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. " 2\n" .. old_task )
                    -- Close the file handle
                    io.close( file )
                end

                file = nil
                old_task = string.sub(old_task,14,-2)
                if(old_task ~= "") then
                    print('true')
                    old_task = ","..old_task
                end
                -- print(old_task)
                new_contents = "local task = {}\ntask.list = {'"..title_name.."'"..old_task.."}\nreturn task"
                file = nil
                errorString = nil
                old_task = nil


                --------------- Write back to file: done ---------------------

                file,errorString = io.open(path,"w")

                if file then
                    print('writing')
                    file:write(new_contents)
                    print('done')
                    io.close( file )
                else
                    print('false')
                    print(file)
                    print( "File error: " .. errorString )
                end
                file = nil
                errorString = nil
                new_contents = nil
            end
            print("finish friday")
--------------------------Saturday----------------------------
            if(selected[7])then
                print('saturday writing')
                path = system.pathForFile("saturday.lua", system.baseDirectory)
                file, errorString = io.open( path, "r" )

                ----------------- read file to gain original data ---------------------
                if not file then
                    -- Error occurred; output the cause
                    print( "File error: " .. errorString )
                    old_task = ""
                else
                    -- Read data from file
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. "\n" .. old_task )
                    old_task = file:read( "*l" )
                    -- Output the file contents
                    print( "Contents of " .. path .. " 2\n" .. old_task )
                    -- Close the file handle
                    io.close( file )
                end

                file = nil
                old_task = string.sub(old_task,14,-2)
                if(old_task ~= "") then
                    print('true')
                    old_task = ","..old_task
                end
                -- print(old_task)
                new_contents = "local task = {}\ntask.list = {'"..title_name.."'"..old_task.."}\nreturn task"
                file = nil
                errorString = nil
                old_task = nil


                --------------- Write back to file: done ---------------------

                file,errorString = io.open(path,"w")

                if file then
                    print('writing')
                    file:write(new_contents)
                    print('done')
                    io.close( file )
                else
                    print('false')
                    print(file)
                    print( "File error: " .. errorString )
                end
                file = nil
                errorString = nil
                new_contents = nil
            end
            print("finish saturday")

        selected = {nil,nil,nil,nil,nil,nil,nil}




        end

--------------------------------------------------------------------------------------------

        sunday = widget.newButton({
            x = display.contentWidth/2.0,
            y = 100,
            height = 40,
            shape = "roundedRect",
            id = "button1",
            label = "Sunday",
            onRelease =  selectSunday,
            fillColor = {default={0.9,0,0}, over = {1,0,0}},
            strokeColor = {1,1,1},
            labelColor = {default = {1,1,1}}
        })

        monday = widget.newButton({
            x = display.contentWidth/2.0,
            y = sunday.y+sunday.height+5,
            height = 40,
            shape = "roundedRect",
            id = "button2",
            label = "Monday",
            onRelease =  selectMonday,
            fillColor = {default={1,0.9,0.2}, over = {1,0.8,0}},
            strokeColor = {1,1,1},
            labelColor = {default = {1,1,1}}
        })

        tuesday = widget.newButton({
            x = display.contentWidth/2.0,
            y = monday.y+monday.height+5,
            height = 40,
            shape = "roundedRect",
            id = "button3",
            label = "Tuesday",
            onRelease =  selectTuesday,
            fillColor = {default={1,0.1,0.8}, over = {1,0.1,0.5}},
            strokeColor = {1,1,1},
            labelColor = {default = {1,1,1}}
        })

        wednesday = widget.newButton({
            x = display.contentWidth/2.0,
            y = tuesday.y+tuesday.height+5,
            height = 40,
            shape = "roundedRect",
            id = "button4",
            label = "Wednesday",
            onRelease =  selectWednesday,
            fillColor = {default={0,0.4,0}, over = {0,0.2,0}},
            strokeColor = {1,1,1},
            labelColor = {default = {1,1,1}}
        })

        thursday = widget.newButton({
            x = display.contentWidth/2.0,
            y = wednesday.y+wednesday.height+5,
            height = 40,
            shape = "roundedRect",
            id = "button5",
            label = "Thursday",
            onRelease =  selectThursday,
            fillColor = {default={1,0.4,0}, over = {1,0.2,0}},
            strokeColor = {1,1,1},
            labelColor = {default = {1,1,1}}
        })

        friday = widget.newButton({
            x = display.contentWidth/2.0,
            y = thursday.y+thursday.height+5,
            height = 40,
            shape = "roundedRect",
            id = "button6",
            label = "Friday",
            onRelease =  selectFriday,
            fillColor = {default={0.2,0.8,1}, over = {0.2,0.8,0.8}},
            strokeColor = {1,1,1},
            labelColor = {default = {1,1,1}}
        })

        saturday = widget.newButton({
            x = display.contentWidth/2.0,
            y = friday.y+friday.height+5,
            height = 40,
            shape = "roundedRect",
            id = "button7",
            label = "Saturday",
            onRelease =  selectSaturday,
            fillColor = {default={0.5,0,0.5}, over = {0.3,0,0.3}},
            strokeColor = {default = {1,1,1}},
            labelColor = {default = {1,1,1}}
        })


----------------------------------------------------
        submit = widget.newButton({
            x = display.contentWidth/2.0,
            y = display.contentHeight - saturday.height,
            height = 40,
            shape = "roundedRect",
            label = "Submit",
            fillColor = {default={0,0.1,0.8}, over = {0,0.1,0.6}},
            onPress = submitTask,
            onRelease = goHome,
            strokeColor = {1,1,1},
            labelColor = {default = {1,1,1}}
        })

        sceneGroup:insert(sunday)
        sceneGroup:insert(monday)
        sceneGroup:insert(tuesday)
        sceneGroup:insert(wednesday)
        sceneGroup:insert(thursday)
        sceneGroup:insert(friday)
        sceneGroup:insert(saturday)

        sceneGroup:insert(submit)



        -- Process Circle
        --[[
        Create circle to be used for displaying the current state of the process
        --]]




        ---------------------------------


        -- Create bottom menu bar

end


-- show()
function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase
    --[[
    imgDir contains the address of the image folder inside the Asset folder
    --]]

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
        -- display.contentWidth
        params = event.params
        title_name = params.courseTitle
        headline = update_headline()
        sceneGroup:insert(headline)


    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    if buttonSearch then
        buttonSearch:removeSelf()
        buttonSearch = nil
    end
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------
-- Scene function listeners
-- -----------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------

return scene
