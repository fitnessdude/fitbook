local composer = require( "composer" )
local widget = require("widget")
local scene = composer.newScene()
local com = require "common"
local tasksList = require("taskList")
local myTimer
local resetBtn
local startBtn
local myText

local myTimer;


local function addSetText(obj)
  function obj:setText(txt,align)
    local a = align
    local oldX = self.x;
    local oldY = self.y;
    self.text = txt;
    -- self:setReferencePoint(a);
    self.x = oldX;
    self.y = oldY;
  end
end

--[[
imgDir contains the address of the image folder inside the Asset folder
--]]
local imgDir = "Assets/Images/"

local function goSearch()
  composer.gotoScene("Scenes.searchPage", "slideRight", 300 )
  return true
end

local function goDaySelect(courseTitle)
    local options = {
      effect = "slideLeft",
      time = 300,
      params = { courseTitle = courseTitle }
  }
  composer.gotoScene("Scenes.daySelectPage",options)
  return true

end

--[[
assigns the return value to the field onRelease.
--]]
local function newclosure(func, ...)
    local args = {...}
    return function()
        return func(unpack(args));
    end
end

local function update_headline()
    if headline ~= nil then headline:removeSelf() print("reload headline") end
    courseName = "CourseName"
    local headlineOptions = {
      text = params.courseTitle,
      x = display.contentWidth/2,
      y = 50,
      width = 200,
      align = "left",
      font = native.systemFont,
      fontSize = 20
    }
    headline = display.newText(headlineOptions)
    headline:setFillColor( 0, 0, 0 )
    return headline
end

local function create_backButton()
    local backButton = widget.newButton{
        defaultFile = imgDir .. "material-design/png/go-back-left-arrow.png",
        onRelease = newclosure(goSearch),
        width = 35,
        height = 35
    }
    backButton.x = 25;
    backButton.y = 50;
    return backButton
end

local function create_timerOutput()
    local timerOutputOptions = {
      text = "Start Timer",
      x = display.contentWidth/2,
      y = 100
    }
    timerOutput = display.newText(timerOutputOptions)
    timerOutput:setFillColor(0,0,0)
    return timerOutput
end


-- create()
function scene:create( event )
    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
end
-- show()
function scene:show( event )
    params = event.params
    local sceneGroup = self.view
    local phase = event.phase


    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
        --[[
        Create background
        --]]
        local background = display.newImageRect(imgDir .. "background-white.jpg", com.right-com.left, com.bottom - com.top)
        background.x = (com.right-com.left)/2; background.y = (com.bottom-com.top)/2+com.top;
        sceneGroup:insert(background)
        --[[
        Backbutton
        --]]

        print("creating back button")
        backButton = create_backButton()
        sceneGroup:insert(backButton)
        --[[
        Headline
        --]]
        headline = update_headline()
        sceneGroup:insert(headline)

        local function selectDay()
          goDaySelect(params.courseTitle)
        end

        -- add button for movement
        addButton = widget.newButton{
          defaultFile = imgDir .. "button/addReleaseButton.png",
          overFile = imgDir .. "button/addPressButton.png",
          onPress = selectDay,
          width = 40,
          height = 40
        }
        addButton.x = (com.right - 20 - addButton.width/2)
        addButton.y = backButton.y
        sceneGroup:insert(addButton)

        local sheetData1 =
        {
          width=960,
          height=540,
          numFrames=24,
          sheetContentWidth = 3840,
          sheetContentHeight = 3240
        }
        local imageSheet = graphics.newImageSheet( "numbbell/numbbell.png", sheetData1 )

        local sequenceData =
        {
            -- { name="walking", start=1, count=3 },
          {
            name="running",
            frames={ 1, 2, 3, 4, 5, 6,7,9,10,11,12,13,15,16,17,18,19,20,21,22,23,24 },
            time = 2000,
            loopCount = 0
          },
            -- { name="jumping", start=9, count=13, time=300 }
        }

        local character = display.newSprite( imageSheet, sequenceData )
        character.x = display.contentWidth/2
        character.y = display.contentHeight/2 + 200

        character:play()
        sceneGroup:insert(character)


    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end

function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
        if (myTimer) then
            timer.cancel(myTimer)
        end
    end
end

function scene:destroy( event )

    local sceneGroup = self.view

    -- Code here runs prior to the removal of scene's view

end
-- -----------------------------------------------------------------------------
-- Scene function listeners
-- -----------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------

return scene


        -- --[[
        -- Timer output
        -- --]]
        -- print("create timer")
        -- timerOutput = create_timerOutput()
        -- sceneGroup:insert(timerOutput)
        -- --[[
        -- state text
        -- --]]
        -- print("create state Text")
        -- stateText = create_stateText()
        -- sceneGroup:insert(stateText)
        -- addSetText(timerOutput);
        -- addSetText(stateText);
        -- --[[
        -- Create timer
        -- --]]
        --   myTimer = timer.performWithDelay(500, function(e)
        --     timerOutput:setText(10-e.count);
        --     print(e.count)
        --     if(e.count == 10) then
        --       print('cancel')
        --       timer.cancel(myTimer);
        --       myTimer = nil;
        --       stateText:setText("Finished");
        --       local alert = native.showAlert( "Fitness", "Time's Up", { "OK" }, nil)
        --     end
        --   end,10);

        -- timer.pause(myTimer);

        -- local paused = true;
        -- local function touchHandler(e)
        --   if(myTimer) then
        --   if(e.phase == "ended" or e.phase == "cancelled") then
        --     if(paused == false) then
        --       timer.pause(myTimer);
        --       stateText:setText("Paused");
        --       paused = true;
        --     elseif (paused == true) then
        --       timer.resume(myTimer);
        --       stateText:setText("Running");
        --       paused = false;
        --     end
        --   end
        -- end
        -- end
              -- Runtime:addEventListener("touch", touchHandler);
