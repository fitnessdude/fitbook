local composer = require( "composer" )
local widget = require("widget")
local scene = composer.newScene()
local com = require "common"
local tasksList

local sun = require("sunday")
local mon = require("monday")
local tue = require("tuesday")
local thu = require("wednesday")
local wed = require("thursday")
local fri = require("friday")
local sat = require("saturday")

local tasksTableView
local headline

local function goSearch()
        composer.gotoScene("Scenes.searchPage", "slideLeft", 300 )
        local currScene = composer.getSceneName( "current" )
        composer.gotoScene( currScene )
    return true
end
local function goCourse(courseTitle)
  local options = {
      effect = "slideLeft",
      time = 300,
      params = { courseTitle = courseTitle }
  }
  composer.gotoScene("Scenes.coursePage", options)
  return true
end
local function update_headline()
    if headline ~= nil then headline:removeSelf() print("reload headline") end
    print("Create headline for the selected day ")
    if params.day ~= nil then
      print("Found day param " .. params.day)
    else
      print("Day param not found")
    end
    local headlineOption = {
      text = "Tasks for  " .. params.day,
      x = display.contentWidth/2,
      y = 50,
      width = 200,
      align = "left",
      font = native.systemFont,
      fontSize = 24
    }
    headline = display.newText(headlineOption)
    return headline
end

-- -----------------------------------------------------------------------------
-- Scene functions
-- -----------------------------------------------------------------------------

-- create()
function scene:create( event )

    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
end

-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase
    params = event.params

    --[[
    imgDir contains the address of the image folder inside the Asset folder
    --]]
    local imgDir = "Assets/Images/"

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
        --[[
        Create background
        --]]
        local background = display.newImageRect(imgDir .. "background.jpg", com.right-com.left, com.bottom - com.top)
        background.x = (com.right-com.left)/2; background.y = (com.bottom-com.top)/2+com.top;
        sceneGroup:insert(background)
        --[[
        insert bottom menu bar
        --]]
         local menuBar = display.newGroup()

        -- buttonProfile = widget.newButton{
        --     defaultFile = imgDir .. "button/userButton.png",
        --     width = display.contentWidth/4.0,
        --     height = display.contentWidth/4.0,
        -- }
        -- buttonProfile.y = com.bottom-buttonProfile.height/1.2
        -- menuBar:insert(buttonProfile)
        --
        buttonHome = widget.newButton{
            defaultFile = imgDir .. "button/searchButton.png",
            onRelease = goSearch,
            width = display.contentWidth/4.0,
            height = display.contentWidth/4.0
        }
        buttonHome.y = com.bottom-buttonHome.height/2
        buttonHome.x = display.contentWidth - 40
        menuBar:insert(buttonHome)
        if(params.day == "Sunday") then
          tasksList = sun.list
        elseif(params.day == "Monday") then
          tasksList = mon.list
        elseif(params.day == "Tuesday") then
          tasksList = tue.list
        elseif(params.day == "Wednesday") then
          tasksList = wed.list
        elseif(params.day == "Thursday") then
          tasksList = thu.list
        elseif(params.day == "Friday") then
          tasksList = fri.list
        elseif(params.day == "Saturday") then
          tasksList = sat.list
        end

        --
        -- buttonSearch = widget.newButton{
        --     defaultFile = imgDir .. "button/searchButton.png",
        --     onRelease = goSearch,
        --     width = display.contentWidth/4.0,
        --     height = display.contentWidth/4.0
        -- }
        -- buttonSearch.y = buttonProfile.y
        -- buttonSearch.x = buttonHome.x + buttonHome.width + buttonProfile.x
        -- menuBar:insert(buttonSearch)

        sceneGroup:insert(menuBar)

        --[[
        Headline
        --]]

        headline = update_headline()
        sceneGroup:insert(headline)

        --[[
        insert daily task list
        --]]

        tasksTableView = create_tasks_table()
        sceneGroup:insert(tasksTableView)
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end

function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen

    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view

    -- Code here runs prior to the removal of scene's view
end



local function getRowTitle(row)
    return tasksList[row]
end

local function onRowRender( event )
    -- Get reference to the row group
    local row = event.row

    -- Cache the row "contentWidth" and "contentHeight" because the row bounds can change as children objects are added
    local rowHeight = row.contentHeight
    local rowWidth = row.contentWidth

    local rowTitle = display.newText( row, getRowTitle(row.index), 0, 0, nil, 24 )

    rowTitle:setFillColor( 0 )
    -- Align the label left and vertically centered
    rowTitle.anchorX = 0
    rowTitle.x = 0
    rowTitle.y = rowHeight * 0.5
end

local function onRowTouch( event )
    local target = event.target
    local index = target.index
    goCourse(getRowTitle(index))
end

function create_tasks_table()
  --[[
  list view for search result
  --]]
  -- Create the table view widget
  local tableView = widget.newTableView(
      {
          left = 0,
          top = 100,
          height = 330,
          width = 320,
          onRowRender = onRowRender,
          onRowTouch = onRowTouch,
          listener = scrollListener
      }
  )
  -- Insert 40 rows
  for i = 1, #tasksList do

      local isCategory = false
      local rowHeight = 50
      local rowColor = { default={ 1, 1, 1 }, over={ 1, 0.5, 0, 0.2 } }
      local lineColor = { 0.5, 0.5, 0.5 }

      -- Insert a row into the tableView
      tableView:insertRow(
          {
              isCategory = isCategory,
              rowHeight = rowHeight,
              rowColor = rowColor,
              lineColor = lineColor,
              params = {}  -- Include custom data in the row
          }
      )
  end
  return tableView
end
-- -----------------------------------------------------------------------------
-- Scene function listeners
-- -----------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------

return scene
