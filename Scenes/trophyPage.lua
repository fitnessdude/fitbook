local composer = require( "composer" )
local widget = require("widget")
local scene = composer.newScene()
local com = require "common"



--[[
imgDir contains the address of the image folder inside the Asset folder
--]]
local imgDir = "Assets/Images/"

local function goProfile()
        composer.gotoScene("Scenes.profilePage", "crossFade", 300 )
    return true
end
--[[
assigns the return value to the field onRelease.
--]]


local function create_backButton()
    local backButton = widget.newButton{
        defaultFile = imgDir .. "material-design/png/go-back-left-arrow.png",
        onRelease = goProfile,
        width = 35,
        height = 35
    }
    backButton.x = 25;
    backButton.y = 50;
    return backButton
end

-- create()
function scene:create( event )
    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
end
-- show()
function scene:show( event )
    params = event.params
    local sceneGroup = self.view
    local phase = event.phase


    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
        --[[
        Create background
        --]]
        local background = display.newImageRect(imgDir .. "background-white.jpg", com.right-com.left, com.bottom - com.top)
        background.x = (com.right-com.left)/2; background.y = (com.bottom-com.top)/2+com.top;
        sceneGroup:insert(background)
        --[[
        Backbutton
        --]]
        print("creating back button")
        backButton = create_backButton()
        sceneGroup:insert(backButton)
        --[[
        Headline
        --]]
        local soon = display.newImageRect(imgDir .. "coming-soon.png",background.width,background.height/2)
        soon.x = (com.right-com.left)/2; soon.y = com.bottom - soon.height/2 - buttonHome.height - 30
        sceneGroup:insert(soon)


    --Button Functions




    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end

function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
        if (myTimer) then
            timer.cancel(myTimer)
        end
    end
end

function scene:destroy( event )

    local sceneGroup = self.view

    -- Code here runs prior to the removal of scene's view

end
-- -----------------------------------------------------------------------------
-- Scene function listeners
-- -----------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------

return scene
