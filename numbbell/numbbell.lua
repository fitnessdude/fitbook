--
-- created with TexturePacker (http://www.codeandweb.com/texturepacker)
--
-- $TexturePacker:SmartUpdate:476b0c2a82d69cc98e0d0858e4f8f817:e97b1025ea48268993589d7b6ee68bbd:aeab16db8afc87e49b6e87fefda89f7a$
--
-- local sheetInfo = require("mysheet")
-- local myImageSheet = graphics.newImageSheet( "mysheet.png", sheetInfo:getSheet() )
-- local sprite = display.newSprite( myImageSheet , {frames={sheetInfo:getFrameIndex("sprite")}} )
--

local SheetInfo = {}

SheetInfo.sheet =
{
    frames = {
    
        {
            -- benchPress.0001
            x=0,
            y=0,
            width=960,
            height=540,

        },
        {
            -- benchPress.0006
            x=960,
            y=0,
            width=960,
            height=540,

        },
        {
            -- benchPress.0011
            x=1920,
            y=0,
            width=960,
            height=540,

        },
        {
            -- benchPress.0016
            x=2880,
            y=0,
            width=960,
            height=540,

        },
        {
            -- benchPress.0021
            x=0,
            y=540,
            width=960,
            height=540,

        },
        {
            -- benchPress.0026
            x=960,
            y=540,
            width=960,
            height=540,

        },
        {
            -- benchPress.0031
            x=1920,
            y=540,
            width=960,
            height=540,

        },
        {
            -- benchPress.0036
            x=2880,
            y=540,
            width=960,
            height=540,

        },
        {
            -- benchPress.0041
            x=0,
            y=1080,
            width=960,
            height=540,

        },
        {
            -- benchPress.0046
            x=960,
            y=1080,
            width=960,
            height=540,

        },
        {
            -- benchPress.0051
            x=1920,
            y=1080,
            width=960,
            height=540,

        },
        {
            -- benchPress.0056
            x=2880,
            y=1080,
            width=960,
            height=540,

        },
        {
            -- benchPress.0061
            x=0,
            y=1620,
            width=960,
            height=540,

        },
        {
            -- benchPress.0066
            x=960,
            y=1620,
            width=960,
            height=540,

        },
        {
            -- benchPress.0071
            x=1920,
            y=1620,
            width=960,
            height=540,

        },
        {
            -- benchPress.0076
            x=2880,
            y=1620,
            width=960,
            height=540,

        },
        {
            -- benchPress.0081
            x=0,
            y=2160,
            width=960,
            height=540,

        },
        {
            -- benchPress.0086
            x=960,
            y=2160,
            width=960,
            height=540,

        },
        {
            -- benchPress.0091
            x=1920,
            y=2160,
            width=960,
            height=540,

        },
        {
            -- benchPress.0096
            x=2880,
            y=2160,
            width=960,
            height=540,

        },
        {
            -- benchPress.0101
            x=0,
            y=2700,
            width=960,
            height=540,

        },
        {
            -- benchPress.0106
            x=960,
            y=2700,
            width=960,
            height=540,

        },
        {
            -- benchPress.0111
            x=1920,
            y=2700,
            width=960,
            height=540,

        },
        {
            -- benchPress.0116
            x=2880,
            y=2700,
            width=960,
            height=540,

        },
    },
    
    sheetContentWidth = 3840,
    sheetContentHeight = 3240
}

SheetInfo.frameIndex =
{

    ["benchPress.0001"] = 1,
    ["benchPress.0006"] = 2,
    ["benchPress.0011"] = 3,
    ["benchPress.0016"] = 4,
    ["benchPress.0021"] = 5,
    ["benchPress.0026"] = 6,
    ["benchPress.0031"] = 7,
    ["benchPress.0036"] = 8,
    ["benchPress.0041"] = 9,
    ["benchPress.0046"] = 10,
    ["benchPress.0051"] = 11,
    ["benchPress.0056"] = 12,
    ["benchPress.0061"] = 13,
    ["benchPress.0066"] = 14,
    ["benchPress.0071"] = 15,
    ["benchPress.0076"] = 16,
    ["benchPress.0081"] = 17,
    ["benchPress.0086"] = 18,
    ["benchPress.0091"] = 19,
    ["benchPress.0096"] = 20,
    ["benchPress.0101"] = 21,
    ["benchPress.0106"] = 22,
    ["benchPress.0111"] = 23,
    ["benchPress.0116"] = 24,
}

function SheetInfo:getSheet()
    return self.sheet;
end

function SheetInfo:getFrameIndex(name)
    return self.frameIndex[name];
end

return SheetInfo
