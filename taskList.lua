local taskList = {}

local baseTaskList = {
"Neck",
"Traps",
"Shoulder",
"Chest",
"Biceps",
"Forearm",
"Abs",
"Quads",
"Calves",
"Traps",
"Lats",
"Triceps",
"Middle Back",
"Lower Back",
"Glutes",
"Hamstrings",
"Yoga",
"Cardio",
"HIIT",
"Strength Training",
"Body Weight"
}


for i = 1, 1 do
	for j = 1, #baseTaskList do
		taskList[#taskList+1] = baseTaskList[j]
	end
end
print("task list has " .. #taskList .. " tasks in  it.")

return taskList
