local movementList = {}


local basemovementList = {
"Goblet Squat",
"Barbell Back Squat",
"Front Squats",
"Bench Step Up",
"Side Lunge",
"Reverse Lunge",
"Push Ups",
"Barbell Shoulder Press",
"DB Incline Press",
"Pull Ups",
"TRX Body Row",
"Single Arm DB Row"
}


for i = 1, 1 do
	for j = 1, #basemovementList do
		movementList[#movementList+1] = basemovementList[j]
	end
end
print("Movement list has " .. #movementList .. " words in  it.")

return movementList
